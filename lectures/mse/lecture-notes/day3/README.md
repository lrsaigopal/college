10/08/2013
==========

* ```NSObject``` is the superclass
* Protocols similar interfaces in Java
* Dynamic typed/Loosely type
* New additions
    * class and methods
    * message invocation
    * synthesis
    
---
### Syntax of a header file 

```
@interface MyClass : NSObject
{
    int count;
    id data;
    NSString* name;
}
- (id) foo : (int) var; //instance method
+ (MyClass*) bar : (NSString*) param; //class method
@end
```

### Syntax of a implementation file
```
@implementation MyClass
- (id) foo : (int) var {
    //some code
}

+ (MyClass *) bar : (NSString*) param {
    //some code
}
@end
```

---
### Points
* Two types of methods
    * class methods
    * instance methods
* **id** is a type bound dynamically 
* Every class is an treated as a static method of class ```Class```  
    Hence we type ```str = [[NSString alloc] init]```
    
---
### Messaging Syntax
* Messagin Syntax  
method-type return-type function-name 
```
- (void) insertObject:(id)anObject atIndex:(id)index
```
* To call: 
```
[receiver message]
[receiver message:argument]
[receiver message:arg1 and Arg:arg2]
[myArray insertObject:anObject atIndex:0]
```
* Nesting message expressions
