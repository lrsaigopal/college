#import <Foundation/NSObject.h>

@interface Two : NSObject
{
	char buffer[200];
}
- (void) addString : (const char*) string;
- (void) print;
@end