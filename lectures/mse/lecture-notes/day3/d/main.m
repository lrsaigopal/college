#import <Foundation/Foundation.h>
#import "04.h"

int main (int argc, char** argv)
{
	Four* t;
	t = [[Four alloc] init];
	[t setX:4];
	[t setY:7];
	NSLog (@"(%d, %d)\n", [t x], [t y]);
	[t print];
	return 0;
}