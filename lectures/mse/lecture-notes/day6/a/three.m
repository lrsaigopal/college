#import <Foundation/Foundation.h>

int main ()
{
	NSMutableDictionary *nd = [NSMutableDictionary dictionary];

	[nd setObject:@"Hello" forKey:@"World"];
	//[nd forKey:@"World" setObject:@"Hello"];
	NSLog (@"%@\n", [nd objectForKey:@"World"]);

	return 0;
}
