#import <Foundation/Foundation.h>

int main (int argc, char *argv[])
{
	NSMutableArray *arr = [NSMutableArray array];

	[arr addObject:[NSNumber numberWithInt:5]];
	[arr addObject:@"Hello World"];
	NSLog (@"%@\n", arr);

	return 0;
}
