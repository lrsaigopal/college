#import <Foundation/Foundation.h>

int main (int argc, char *argv[])
{
	NSArray *arr = [NSArray arrayWithObjects:(id)@"one",@"two",@"three",nil];

	int count = [arr count];
	NSLog (@"count: %d\n", count);
	
#if 0
	for (int i = 0; i < count; ++ i) {
		NSLog (@"%d: %@\n", i, [arr objectAtIndex:i]);
	}
#else
	NSLog (@"%@\n", arr);
#endif

	int index;
	if ((index = [arr indexOfObject:@"3"]) == NSNotFound) {
		NSLog (@"Element not found in array\n");
	} else {
		NSLog (@"The index is %d\n", index);
	}


	return 0;
}
