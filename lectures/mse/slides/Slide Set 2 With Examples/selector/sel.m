#import <Foundation/Foundation.h>
int main(){
   NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
   
   NSMutableArray *array = [NSMutableArray arrayWithObjects:@"red",@"blue", @"greeen", nil];
   
   SEL message = @selector(addObject:);
   
   if([array respondsToSelector:message]){
      [array performSelector:message withObject:@"yellow"];  // [array addObject:@"yellow"]
   }
   
   NSLog(@"%@", array);   
 
 //-(void)someMethod:(SEL)someSelector;
   
   [pool drain];
   return 0;
}