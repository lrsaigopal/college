// main.m 
#import <Foundation/Foundation.h> 
#import "Mobile.h" 

int main(int argc, const char * argv[]) {

   NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
 
   Mobile *myMob = [[Mobile alloc] initWithModel:@"Galaxy"]; 
   
   // Get the class of an object 
   NSLog(@"%@ is an instance of the %@ class", [myMob _model], [myMob class]); 
 
   // Check an object against a class and all subclasses 
   if ([myMob isKindOfClass:[NSObject class]]) {
      NSLog(@"%@ is an instance of NSObject or one of its subclasses", [myMob _model]); 
   }
   else {
      NSLog(@"%@ is not an instance of NSObject or one of its subclasses", [myMob _model]); 
   }
    
   // Check an object against a class, but not its subclasses
   if ([myMob isMemberOfClass:[NSObject class]]) {
       NSLog(@"%@ is a instance of NSObject", [myMob _model]); 
   }
   else { 
       NSLog(@"%@ is not an instance of NSObject", [myMob _model]);
   } 
   
   [pool release];  
   
   return 0;

}
