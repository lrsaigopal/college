// Mobile.h 
#import <Foundation/Foundation.h> 

@interface Mobile : NSObject { 

   NSString *_model;
}

@property(nonatomic,retain)  NSString *_model;
  
- (id)initWithModel:(NSString *)aModel; 
 
@end 
