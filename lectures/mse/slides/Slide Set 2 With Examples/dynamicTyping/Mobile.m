// Mobile.m
#import<Foundation/Foundation.h>
#import "Mobile.h"

@implementation Mobile

@synthesize _model;

- (id)initWithModel:(NSString *)aModel { 
      self = [super init]; 
      if (self) { 
         // Any custom setup work goes here 
         _model = aModel; 
      } 
      return self; 
} 
   
@end