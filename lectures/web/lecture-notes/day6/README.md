22-08-2013
=========

### Agenda 
* Local Storage

---
### Local Storage

* Cookies are implemented as key value pairs; Local Storage is implemented as arrays (associative array).
* ```localStorage``` variable on ```window``` object.
* **Note**: In Modernizr everything is lower case. To check for ```localStorage```, perform
```
if (Modernizr.localstorage) {
// yes its present
}
```
* List of functions on ```localStorage```
    * associative index - ```localStorage["key"]```; both get and set possible.
    * ```getItem("key")``` to get an item.
    * ```setItem("key")```
    * ```removeItem("key")``` to remove the item. 
    * ```clear()``` clears all
* Events are fired when set or remove is used. 
    * event fired is ```storage```
    * fired on all open pages from the same domain.
    * ```preventDefault``` can't be applied for this. ie cannot be canceled. Do something or don't do anything.
    * Not fired when simply assigning to same value. Value has to change to fire.
    * Data the ```event``` object contain -
        * ```key``` which key was modified.
        * ```oldValue```
        * ```newValue```
        * ```url``` Which page was responsible for it.
* ```clear``` also fires an event but ```oldValue``` and ```newValue``` both will be ```null```.

---
### Look at

* ```event.key``` when ```clear```  is called. 
