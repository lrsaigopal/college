29-08-2013
=========

### Agenda

* XML

---
### XML contd...

#### Implementation

* ```window.navigator.appName``` for browser name  
Use this to distinguish b/w IE and others  
IE => ```window.navigator.appName == "Microsoft Internet Explorer```
* For IE
    * ```xmldom = new ActiveXObject ("MSXML2.DOMDocument.6.0");``` to create ActiveX XML object
    * Some properties
        * ```xmldom.async = true``` Parse asynchronously
        * ```xmldom.preserveWhiteSpace = false;``` By default false;
        * ```xmldom.validateOnParse = false;```
            * Default in IE is to validate.
    * If asynchronous, we can use the following when the callback occurs.  
    ```xmldom.onreadystatechange = function () {...}```
    * To load XML : ```xmldom.load("file.xml");```
    * The callback function implements the same functionality as that of  AJAX. ie check the ```xmldom.readyState == 4```.
    * If XML parsing failed, even then it'd return with a ```xmldom.readyState == 4``` because parser completed. To check if there were errors, we can use the following properties of ```xmldom.parseError``` . What these properties do are implicit.
        * ```xmldom.parseError.errorCode```
            * ```0``` if Success
        * ```xmldom.parseError.reason```
        * ```xmldom.parseError.line```
    * If everything went well, the DOM will have been built and we can use the following to traverse 
        * ```getElementById```
        * ```getElementByTagName```
        * ```firstChild```
        * ```lastChild```
        * ```nextSibling```
        * ```previousSibling```
    * The nodes have some properties
        * ```nodeName```
        * ```nodeType```
        * ```nodeValue```
        * ```childNodes```
        * ```text``` (IE only)
        * ```xml``` (IE only)
    * ```xmldom.documentElement``` gives the root node.
    * To parse asynchronously -
        * ```xmldom.loadXML(xmlstr);``` where ```xmlstr``` is the string containing the XML.
* Other browsers -
    * What are present and not present?
        * No ActiveXObject
        * no asynchronous call
        * no ```preserveWhiteSpace```
        * ```loadXML ``` not present
        * no ```xml``` property and ```text``` property for nodes
        * no ```readyState```; there is an ```onerror``` for error and ```onload``` for success, events to use instead of ```readyState```
    * To create ```xmldom``` object - 
        *  ```xmldom = document.implementation.createDocument("", "", null)```
    * To imitate the ```text``` property,
        * ```ser = new Serializer();```. Create serializer object.
        * ```xstr = ser.serializeToString(domnode)```. Gives the string.
        * Every browser will have a ```DOMParser()```. So create ```parser = new DOMParser();```.
        * To get the DOM, ```xmldom = parse.parseFromString(xmlstr, "text/xml");```
* **zxml** is a union of IE and other browsers and all features are available
    * Built in object - ```xmldom = zXmlDom.createDocument();``` and proceed.

#### Using XPATH

* Fetch all depts
    * ```root = xmldom.documentElement;```
    * ```xpathstr = "college/depts"```
    * ```nodes = root.selectNodes(xpathstr);```
* Some tricks
    1. ```"//dept"``` - All dept nodes
    2. ```"book[price<400]"``` books with price less than 400.
    3. ```/books::descendant/author``` and ```/books::child/author```
        * ```::``` is called axes. child, descendant, parent, etc
    4. ```"book[@id='001']"``` - obvious with the structure
    
#### Problems

* (Considering class example) Pick out all colleges which offer mechanical.
    * Brute force - Go through all nodes, check for a mech node.
    * Using XPATH
        * ```xpathstr = "college[dept[contains(text(), 'MECH')]];``` - gets the college node
        * ```xpathstr = "college/dept[contains(text(), 'MECH')];``` - gets the dept node of that college.

---