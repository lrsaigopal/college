27-08-2013
=========

### Agenda 

* Offline Browsing
* XML (very little)

---
### Offline Browsing

* App Cache
* The server decides what pages would be cached
* Any data which is volatile cannot be cached
* The server decides what pages can definitely not be cached
* Fallbacks are present
* No JS, only markup
* Check for ```Modernizr.applicationcache```
* Window object ```window.applicationCache``` - events fired to this object
    
#### Implementation

* ```manifest``` attribute in html decides offline browsing
* ```<html manifest=myfile.manifest>```  
the extension must be ```.manifest```
* The content type of ```.manifest``` is ```text/cache-manifest```  
In Apache2, go to httpd.conf and find ```AddType text/cache-manifest .manifest```
* The manifest file is downloaded and read
* Everyfile which is part of the offline browsing process must have that ```manifest``` attribute in ```html```  tag. Why?  
The files which are not listed in ```CACHE:``` are still placed in app cache for future use. If that attribute was not listed then it would have gone to the normal cache which has timeout kind of thins which could delete the page later. Having it stored in app cache makes UX better.
* JS Events. Fired to ```window.applicationCache``` object
    * ```checking/noupdate```
    * ```download```
    * ```progress```
    * ```cached/updated```

#### Cache Manifest 

* The first line is ```CACHE MANIFEST```
* Three sections
    1. Cache or Explicit section
    2. Network
    3. Fallback
* Cache Section
    * Section starts by ```CACHE:```
    * Add links one after the other
* Network
    * Section starts by ```NETWORK:```
    * Wildcard ```*```
    * Same as above, add links
* Fallback
    * Used when some cache link could not be downloaded
    * If something in ```CACHE:``` not downloaded, do the ```FALLBACK:```
    *  ```/``` means any file. 
    * Example ```/ /fallback.html``` means download ```/fallback.html``` if any of the files in ```CACHE:``` are not downloaded.
    * Also possible to map individual files
    
---
### XML

* Why XML at client side - Reduce load.
* SAX and DOM
    * Which is better? - Depends on memory, traversing up and down
    * Usually DOM followed by browsers because DOM is widely used
* DOM - one kind in IE (using ActiveX), another with the rest.

#### About XML file

* ```<? version="1.0 ?>```
* Big XMLs can use DTDs
* Well formed (you know what)

#### Implementing 

* ```window.navigator.appName``` for browser name  
Use this to distinguish b/w IE and others  
IE => ```window.navigator.appName == "Microsoft Internet Explorer```
* For IE
    * ```xmldom = new ActiveXObject ("MSXML2.DOMDocument.6.0");``` to create ActiveX XML object
    * Some properties
        * ```xmldom.async = true``` Parse asynchronously
        * ```xmldom.preserveWhiteSpace = false;``` By default false;
        * ```xmldom.validateOnParse = false;```

---
### Look at

* 