17-08-2013
=========

### Agenda 
* Canvas
* Multithreading
* Local Storage (intro)

---
### Canvas (pre)

* Pencil methods
    * ```moveTo()```
    * ```lineTo()```
    * ```arc()```
    * ```stroke()```, ```fill()```
    * ```beginPath()```, ```closePath()```
* ink 

---
### Canvas

* Linear Gradient
```
canvasObject = document.getElementById("foo");
context = canvasObject.getContext();

grad = context.createLinearGradient(x1, y1, x2, y2); // These are gradient coods
grad.addColorStop(<value b/w 0 and 1>, "color-name");
grad.addColorStop(<value b/w 0 and 1>, "color-name");
grad.addColorStop(<value b/w 0 and 1>, "color-name");

context.fillStyle(grad);
cont.fillRect(x1, y1, x2, y2);
``` 

* Radial Gradient
```
canvasObject = document.getElementById("foo");
context = canvasObject.getContext();

grad = context.createRadialGradient(center_x1, center_y1, radius_1, 
                                    center_x2, center_y2, radius_2);
grad.addColorStop(<value b/w 0 and 1>, "color-name");
grad.addColorStop(<value b/w 0 and 1>, "color-name");
grad.addColorStop(<value b/w 0 and 1>, "color-name");

context.fillStyle(grad);
cont.fillRect(x1, x2, y1, y2);
``` 