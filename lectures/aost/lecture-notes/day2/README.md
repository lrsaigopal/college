Day 2 - 12-08-1992
===============

### What to look in a Server
* Threads vs children -
    * In general yes, might be better 
    * but sometimes depending on the machine, threads may be much more inefficient 
    * Thread safe code critical
* accept zombie problem
* select system call
    * Using macros FD_ZERO, etc.. .checkout *man 2 select*

### Look at
* PF_NET

### ToDo
* In the exercise, make sure you address all the required signal problems