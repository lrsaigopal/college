Day 1 - 06-09-2013
===============

## Resources
* [Website](http://ise.pesit.pes.edu/ISE/2013-CS436/Prog)  
Username: ISE  
Password:  PESIT  
* Email: rprustagi@pes.edu

---
## TCP Communication

### Important notes
* Network uses BigEndian mode network byte order

---
### TCP Connection concepts

Order of calls in a TCP server2

* socket() 
* bind ()
* listen () 
* accept ()
* close ()



### Look at:
```
sudo tcpdump -n -i any port xxxxx
netstat -natp
```

Six flags

* SYN
* ACK
* RST
* FIN  
* URG  
* PUSH

---
### Q. New clients initiate TCP connection. Server already has one, Does it accept the new?
Yes. But accept has not gone through, so comm damaar.

---
### Q. Client initiates a connection when server initiates listen() but not accept()


---
### Q. Client closes connection duing send/recv process. What happens to server recv? 
returns 0. Indicating Client has closed connection

---
### Look at: 
```
strace
```

---
* ``` SO_REUSEADDR``` flag to make bind succeed on the 2nd invocation
* use ```getopt``` to read position independent arguments 

---
### Exercise
Check in the slides.

---
### ToDo:
* Look at slides.
* Experiment with TCP program structure (in C)
* Solve Exercise

