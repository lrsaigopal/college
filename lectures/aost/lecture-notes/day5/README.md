26-08-2013
==========

### Agenda
* Web server overview
* Boa Web Server

---
### HTTP Protocol
  
* HTTP RFC 2616 
* HTTP 1.1 vs HTTP v1.0?

#### The Protocol
* Request Line
    * method
    * path
    * HTTP version
* Request Headers
    * Some separator with Request Line
* Request Data
* Response Status Line
    * HTTP Version
    * Code
    * Text value
* Response Headers
* Response Data
    
---
### Boa Web Server

* [Website](http://www.boa.org)
* config in ```boa.conf```
* Goals
    * Speed
    * Security
    
---
### Implementation

* 3 queues
    * all are doubly linked lists
    * ready Q
        * for further processing
        * round robin
    * blocked Q
        * waiting for any data dependence
    * free Q
* implements the ```select``` method
* time management through own variables, no alarms
* listener FD always in blocked queue
* ```mmap``` for reading
* FSM based

---
### Look at

* Look at HTTP 1xx codes
* HTTP v1.1 vs HTTP v1.0
* cgi-bin
* ```mmap()``` system call

---
### Assignments

* Draw the FSM Boa uses