/* include child_make */
#include	"tcpcommon.h"
#include	"child.h"

extern Child_info	*g_child_info;
void	child_main(int, int);
void	process_child(int);

pid_t child_make(int ii, int listenfd) {
	int		sockfd[2];
	pid_t	pid;
	int		status;

	/* create a socket pair between parent and child.
	 * parent will close one end and child will close another end
	 */
	status = socketpair(AF_LOCAL, SOCK_STREAM, 0, sockfd);

	if ( (pid = fork()) > 0) {
		status = close(sockfd[1]); /* parent closed one end */
		g_child_info[ii].child_pid = pid;
		g_child_info[ii].child_pipefd = sockfd[0];
		g_child_info[ii].child_status = CHILD_FREE;
		return(pid);		/* parent */
	}

	status = dup2(sockfd[1], STDERR_FILENO);	/* child's stream pipe to parent */
	status = close(sockfd[0]); /* child closes other end */
	status = close(sockfd[1]); /* already dup(), so can be closed as well */
	status = close(listenfd);  /* child does not need this open */
	child_main(ii, listenfd);	/* never returns */
}
/* end child_make */

/* -------------------------------------- */
/* include child_main */
void child_main(int ii, int listenfd) {
	char		c;
	int			connfd;
	ssize_t		n;
	int		status;

	printf("child num %d with %ld starting\n", ii, (long) getpid());
	for ( ; ; ) {
		if ( (n = read_fd(STDERR_FILENO, &c, 1, &connfd)) == 0) {
			err_quit("read_fd returned 0");
		}
		process_child(connfd);				/* process request */
		status = close(connfd);

		n = write(STDERR_FILENO, "", 1);	/* tell parent we're ready again */
	}
}
/* end child_main */
