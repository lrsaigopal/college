/* include read_fd */
#include	<stdarg.h>		/* ANSI C header file */
#include	<syslog.h>		/* for syslog() */
#include	"tcpcommon.h"

#define	MAXN	16384		/* max # bytes client can request */
#define	MAXLINE	1024		/* max # bytes client can request */

/* -------------------------------------- */
/* Fatal error unrelated to system call
 * Print message and terminate */

void err_quit(const char *fmt, ...) {
	va_list		ap;

	va_start(ap, fmt);
	fprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

/* -------------------------------------- */
ssize_t read_fd(int fd, void *ptr, size_t nbytes, int *recvfd) {
	struct msghdr	msg;
	struct iovec	iov[1];
	ssize_t			n;

	union {
	  struct cmsghdr	cm;
	  char				control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr	*cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	iov[0].iov_base = ptr;
	iov[0].iov_len = nbytes;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	if ( (n = recvmsg(fd, &msg, 0)) <= 0) {
		return(n);
	}

	if ( (cmptr = CMSG_FIRSTHDR(&msg)) != NULL &&
	    cmptr->cmsg_len == CMSG_LEN(sizeof(int))) {
		if (cmptr->cmsg_level != SOL_SOCKET) {
			err_quit("control level != SOL_SOCKET");
		}
		if (cmptr->cmsg_type != SCM_RIGHTS) {
			err_quit("control type != SCM_RIGHTS");
		}
		*recvfd = *((int *) CMSG_DATA(cmptr));
	} else {
		*recvfd = -1;		/* descriptor was not passed */
	}

	return(n);
}
/* end read_fd */
/* -------------------------------------- */

ssize_t write_fd(int fd, void *ptr, size_t nbytes, int sendfd) {
	struct msghdr	msg;
	struct iovec	iov[1];

	union {
	  struct cmsghdr	cm;
	  char				control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr	*cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	cmptr = CMSG_FIRSTHDR(&msg);
	cmptr->cmsg_len = CMSG_LEN(sizeof(int));
	cmptr->cmsg_level = SOL_SOCKET;
	cmptr->cmsg_type = SCM_RIGHTS;
	*((int *) CMSG_DATA(cmptr)) = sendfd;

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	iov[0].iov_base = ptr;
	iov[0].iov_len = nbytes;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	return(sendmsg(fd, &msg, 0));
}
/* end write_fd */
/* -------------------------------------- */

void process_child(int sockfd) {
	ssize_t		n;
	char		line[MAXLINE], result[MAXN];

	for ( ; ; ) {
		if ( (n = read(sockfd, line, MAXLINE)) == 0) {
			return;		/* connection closed by other end */
		}
		n = write(sockfd, line, n);
	}
}
/* -------------------------------------- */

