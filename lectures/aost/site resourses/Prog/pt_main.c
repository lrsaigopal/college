/* src: https://www.comp.glam.ac.uk/pages/staff/hggross/IntelligentTimingAnalysis_HTML/node20.html */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <syscall.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/user.h>

void main (int argc, char  *argv) {

  long long counter = 1; // machine instruction counter
  int  wait_val;         // child's return value
  int  pid;              // child's process id
  struct user_regs_struct regs;
  long inst;

  switch (pid=fork()) {
    case -1: 
		perror("fork"); 
		break;
    case 0:     // child process starts
             ptrace(0,0,0,0);
                // must be called in order to allow the
                // control over the child process
             execl("./prog", "testprog", "111", "333",  NULL);

                // executes the testprogram and causes
                // the child to stop and send a signal 
                // to the parent, the parent can now
                // switch to PTRACE_SINGLESTEP
             break;
                // child process ends
    default:    // parent process starts
             wait(&wait_val);
                // parent waits for child to stop (execl)
             //if (ptrace(PTRACE_ATTACH, pid, 0, 0) != 0)
             if (ptrace(PTRACE_SINGLESTEP,pid,0,0) != 0) {
               perror("ptrace");
			 }
                // switch to singlestep tracing and 
                // release child
             wait(&wait_val);
                // wait for child to stop at next instruction
             while (wait_val == 1407) {
               counter++;
			   if ((counter % 10000) == 0) {
                 printf("Counter: %llu\n", counter);
			   }
               if (ptrace(PTRACE_SINGLESTEP, pid, 0, 0) != 0) {
                 perror("ptrace");
			   }
               wait(&wait_val);
			   //ptrace(PTRACE_GETREGS, pid, NULL, &regs);
			   //inst = ptrace(PTRACE_PEEKTEXT, pid, regs.rip, NULL);
			   //printf("EIP: %lx Ins exec: %lx\n", regs.rip, inst);
             }
               // continue to stop, wait and release until
               // the child is finished; wait_val != 1407
               // Low=0177L and High=05 (SIGTRAP)
    } // end of switch
	printf("Counter: %llu\n", counter);
} // end of main

