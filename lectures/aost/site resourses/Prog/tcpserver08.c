/**
 * on receipt of each new request, server spawns a child and
 * thus each client is handled by a separate child process.
 * When a child exit, it is handled by parent due to signal handler
 */

#include "tcpcommon.h"
#include <sys/wait.h>
#include <signal.h>
#include <sys/time.h>

void str_echo(int fd);

static void child_handler(int signo) {
    pid_t child_pid; 
	int status; 
	do {
		child_pid = waitpid(-1, &status, WNOHANG);
	} while ( child_pid != -1 ); /* loop will continue as longs as there are processes that exited */
	signal(SIGCHLD, child_handler);
}

int
main(int argc, char **argv) {
	int     listenfd, connfd;
	pid_t   childpid;
	struct sockaddr_in cliaddr, servaddr;
	socklen_t clilen;
	int	child_status;
	int opt;
	char *ip_addr = (char *) NULL;
	short port = 0;

	while ((opt = getopt(argc, argv, "i:p:")) != -1) {
		switch (opt) {
		case 'i':
			ip_addr = optarg;
			break;

		case 'p':
			port = atoi(optarg);
			break;

		default:
			fprintf(stderr, "Usage: %s -i ipaddr -p port\n", argv[0]);
			exit(1);
			break;
		}
	}

	if ((port == 0) || (ip_addr == (char *)NULL)) {
		fprintf(stderr, "either ipaddr or port number not specified or invalid value\n");
		exit(1);
	}

	/* defines signal handler for child */
	/* ensure all zombie processes are released */
	signal(SIGCHLD, child_handler);

	listenfd = socket (AF_INET, SOCK_STREAM, 0);

	bzero((void *)&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr(ip_addr);
	servaddr.sin_port = htons(port);

	bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	listen(listenfd, LISTENQ);

	clilen = sizeof(cliaddr);
	for (; ; ) {
		connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
		if ( (childpid = fork()) == 0) { /* child process */
			close(listenfd);    /* close listening socket */
			str_echo(connfd);   /* process the request */
			exit (0);
		} else {
			printf("New request by process %d from %s:%d\n", childpid, inet_ntoa(cliaddr.sin_addr), ntohs(cliaddr.sin_port));
		}
	}
}

void str_echo(int connfd) {
	char buf[BUFFSIZE];
	int size;

	bzero(buf, sizeof(buf));
	while ( (size = recv(connfd, buf, sizeof(buf), 0) > 0)) {;
		send(connfd, buf, strnlen(buf, sizeof(buf)), 0);
		bzero(buf, sizeof(buf));
	}
	close(connfd);
}
