#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <stdlib.h>
#include <errno.h>


/* server parameters */
#define SERVER_PORT	12345
#define LISTENQ     2         /* 2nd argument to listen () */
#define BUFFSIZE    8192         /* buffer size for reads and writes */
