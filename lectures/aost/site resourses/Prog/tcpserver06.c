/**
 * on receipt of each new request, server spawns a child and
 * thus each client is handled by a separate child process.
 * Till server exits, the child will exit, but will remain a zombie process
 */

#include      "tcpcommon.h"
void str_echo(int fd);

int
main(int argc, char **argv) {
	int     listenfd, connfd;
  pid_t   childpid;
  struct sockaddr_in cliaddr, servaddr;
  socklen_t clilen;

	listenfd = socket (AF_INET, SOCK_STREAM, 0);

	bzero((void *)&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl (INADDR_ANY);
	servaddr.sin_port = htons(SERVER_PORT);

	bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

	listen(listenfd, LISTENQ);

	clilen = sizeof(cliaddr);
	for (; ; ) {
		connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
		if ( (childpid = fork()) == 0) { /* child process */
			printf("Processing new request from %s:%d\n", inet_ntoa(cliaddr.sin_addr), ntohs(cliaddr.sin_port));
			close(listenfd);    /* close listening socket */
			str_echo(connfd);   /* process the request */
			exit (0);
		}
	}
}

void str_echo(int connfd) {
	char buf[BUFFSIZE];
	int size;


	bzero(buf, sizeof(buf));
	while ( (size = recv(connfd, buf, sizeof(buf), 0) > 0)) {;
		send(connfd, buf, strnlen(buf, sizeof(buf)), 0);
		bzero(buf, sizeof(buf));
	}
	close(connfd);
}
