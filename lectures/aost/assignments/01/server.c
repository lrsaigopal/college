#include "tcpcomm.h"

int create_socket (int domain, int type, int protocol)
{
	int ret = socket (domain, type, protocol);
	if (ret == -1) {
		perror ("socket");
		exit (ERROR_SOCKET);
	}
	return ret;
}

void so_reuseaddr (int sfd, int level)
{
	int val = 1;
	if (setsockopt (sfd, level, SO_REUSEADDR, &val, sizeof (int)) == -1) {
		perror ("setsockopt");
		exit (ERROR_SETSOCKOPT);
	}
}

void init_sockaddr (struct sockaddr_in *ptr_sockaddr, int domain, uint32_t addr, uint16_t port)
{
	ptr_sockaddr->sin_family = domain;
	ptr_sockaddr->sin_addr.s_addr = htonl (addr);
	ptr_sockaddr->sin_port = htons (port);
}

void bind_sockaddr (int sfd, struct sockaddr_in* ptr_sockaddr, socklen_t size)
{
	if (bind (sfd, (struct sockaddr*) ptr_sockaddr, size) == -1) {
		perror ("bind");
		exit (ERROR_BIND);
	}
}

void listen_socket (int sfd, int backlog)
{
	if (listen (sfd, backlog) == -1) {
		perror ("listen");
		exit (ERROR_LISTEN);
	}
}

int accept_conn (int sfd, struct sockaddr_in* ptr_sockaddr, socklen_t *ptr_size)
{
	int ret;
	ret = accept (sfd, (struct sockaddr*) ptr_sockaddr, ptr_size);
	if (ret == -1) {
		perror ("accept");
		exit (ERROR_ACCEPT);
	}
	return ret;
}

void close_conn (int cfd)
{
	if (close (cfd)) {
		perror ("close");
		exit (ERROR_CLOSE);
	}
}

void process_conn (int cfd)
{
	char buf[256] = {0};
	ssize_t len;

	while ((len = recv (cfd, buf, sizeof (buf), 0)) > 0) {
		if (send (cfd, buf, sizeof (buf), 0) == -1) {
			perror ("recv");
			exit (ERROR_RECV);
		}

		bzero (buf, sizeof (buf));
	}
	if (len == -1) {
		perror ("recv");
		exit (ERROR_RECV);
	}
}

void runserver (int port)
{
	int sfd, cfd;
	struct sockaddr_in saddr, caddr;
	int slen = sizeof(saddr), clen = sizeof (caddr);

	sfd = create_socket (AF_INET, SOCK_STREAM, 0);
	so_reuseaddr (sfd, SOL_SOCKET);
	init_sockaddr (&saddr, AF_INET, INADDR_ANY, port);
	bind_sockaddr (sfd, &saddr, slen);
	listen_socket (sfd, 5);

	for (;;) {
		cfd = accept_conn (sfd, &caddr, &clen);
		if (fork ()) {
		} else {
			process_conn (cfd);
			close_conn (cfd);
			exit (0);
		}
	}
}

int main (int argc, char* argv[])
{
	int opt;
	int port;

	while ((opt = getopt (argc, argv, "a:p:")) != -1) {
		switch (opt) {
			case 'a':
			break;
			case 'p':
				port = atoi (optarg);
			break;
		}
	}

	runserver (port);
	return 0;
}