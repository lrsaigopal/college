#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

pthread_t t1, t2;

void* foo (void* ptr)
{
	//pthread_create (&t2, 0, bar, 0);
	sleep (1);
	printf ("foo: %d\n", *(int*)ptr);
}

void bar (void* ptr)
{
	sleep (5);
	printf ("bar\n");
}

int main ()
{
	int n = 10;;
	pthread_create (&t1, 0, foo, &n);
	pthread_exit (0);
	return 0;
}
