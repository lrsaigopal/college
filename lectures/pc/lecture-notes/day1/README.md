17-08-2013
==========

### Notes - pthread in C
* interface, not OO, procedural, shared memory model
* mem access time same for all threads
* When a process terminates, all threads die. ie exists in the context of a process
* No parents; no orphan threads; no hierarchy 
* Signal disposition shared; signal blocking is not shared
* ```pthread_exit``` to exit threads; main thread can also be exited without exiting the process
* cleanup handlers for threads
    * push some functions on stack; called in the reverse order of pushing during cleanup
* What all can be thread functions?  
    * C function
    * static function of a C++ class
    * call non static function through a static wrapper

---
### ```pthread_create()```
* Arguments
    * thread object
    * object for initialization of thread
    * callback mechanism
        * function whose signature is ```void func (void*);```
    * argument for function
* returns 0 on success

---
### Questions/ToDo
* Look at zombie threads
* Does the main stack go away if ```main()``` exits? 
* why join takes a pthread_t object rather than pthread_t* ?