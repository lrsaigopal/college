#define _XOPEN_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

typedef struct {
	size_t low;
	size_t high;
	size_t count;
} targs_t;

void parse_args (int argc, char* argv[], size_t *low, size_t *high, int *nt)
{
	size_t opt;

	while ((opt = getopt(argc, argv, "l:h:n:")) != -1) {
		switch (opt) {
		case 'l':
			*low = atoi (optarg);
		break;
		case 'h':
			*high = atoi (optarg);
		break;
		case 'n':
			*nt = atoi (optarg);
		break;
		default:
			printf ("Usage: -l <low> -h <high> [-n <number_of_threads]\n");
		}
	}
}

void* count_prime (void *ptr)
{
	targs_t* args = (targs_t*)ptr;
	size_t low = args->low; size_t high = args->high; size_t count = 0;

	size_t i, j;
	for (i = low; i <= high; ++ i) {
		for (j = 2; i % j != 0; ++ j);
		if (i == j) {
			++ count;
		}
	}
	args->count = count;

	return ptr;
}

void distribute (targs_t args[], size_t low, size_t high, int nt)
{
	size_t range = (high - low) / nt;
	for (size_t i = 0; i < nt; ++ i) {
		args[i].low = low;
		args[i].high = low + range;
		low = low + range + 1;
	}
}

int main (int argc, char *argv[])
{
	if (argc < 2) {
		printf ("No arguments provided!\n");
	}

	size_t low; size_t high; int nt = 1;
	parse_args (argc, argv, &low, &high, &nt);
	targs_t args[nt];
	distribute (args, low, high, nt);
	
	pthread_t threads[nt];

	time_t begin = time(0);
	for (int i = 0; i < nt; ++ i) {
		pthread_create (&threads[i], 0, count_prime, (void*)(&args[i]));
	}
	for (int i = 0; i < nt; ++ i) {
		pthread_join (threads[i], 0);
	}
	time_t end = time(0);
	printf ("time taken: %d\n", end - begin);
	for (int i = 0; i < nt; ++ i) {
		printf ("(%lu, %lu): %lu\n", args[i].low, args[i].high, args[i].count);
	}

	pthread_exit (0);

	return 0;
}
