#include "all.h"

void* pthread_cleanup_foo (void *ptr)
{
	printf ("cleaning up!\n");
	return 0;
}

void* pthread_foo (void *ptr)
{
	pthread_cleanup_push (pthread_cleanup_foo, 0);
	printf ("inside thread\n");
	sleep (3);
	printf ("going out of thread\n");
	pthread_cleanup_pop (1);
	return 0;
}

int main (int argc, char *argv[])
{
	pthread_t t1;

	pthread_create (&t1, 0, pthread_foo, 0);
	sleep (1);
	pthread_cancel (t1);

	pthread_exit (0);

	return 0;
}
