23-08-2013
=========

### Agenda

* Cleanup handlers

---
### Cleanup handlers

* ```pthread_cleanup_push```
* ```pthread_cleanup_pop```
* called when ```pop```'s arugment is ```0```.
* called when thread has ```pthread_exit (0)```
* the ```pthread_cleanup_push``` and ```pthread_cleanup_pop``` are both macros
* both functions must be in the same scope
    * checkout ```gcc -E``` for the reason
   
---
### Notes
  
* ```pthread_kill``` send signals from one thread to another 