#import <Foundation/NSObject.h>

@interface Calculator : NSObject
{
	double x;
	double y;
}
@property double x;
@property double y;

- (double) add;
- (double) sub;
- (double) mul;
- (double) div;

@end