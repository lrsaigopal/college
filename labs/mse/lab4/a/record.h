#import <Foundation/Foundation.h>

@interface Record : NSObject
{
	NSString *name;
	NSString *email;
	NSString *phoneNumber;
	NSString *misc;
}

@property (copy) NSString *name;
@property (copy) NSString *email;
@property (copy) NSString *phoneNumber;

- (id) initWithName:(NSString*)paramName initWithEmail:(NSString*)paramEmail initWithPhoneNumber:(NSString*)paramPhoneNumber;
- (NSString*)description;
@end
