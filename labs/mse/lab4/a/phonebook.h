#import <Foundation/Foundation.h>
#import "record.h"

@interface PhoneBook : NSObject
{
	NSString *phoneBookName;
	NSMutableArray *records;
}

@property (copy) NSString* phoneBookName;

- (id) initWithName:(NSString*)paramPhoneBookName;
- (void) addRecord:(Record*)record;
- (void) removeRecord:(NSString*)name;
- (id) searchRecord:(NSString*)name;
- (NSString*)description;

@end
