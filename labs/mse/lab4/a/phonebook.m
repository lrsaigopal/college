#import <Foundation/Foundation.h>
#import "phonebook.h"

@implementation PhoneBook

@synthesize phoneBookName;

- (id) initWithName:(NSString*)paramPhoneBookName {
	phoneBookName = paramPhoneBookName;
	records = [[NSMutableArray alloc] init];

	return self;
}
- (void) addRecord:(Record*)record {
	[records addObject:record];
}

- (id) searchRecord:(NSString*)name {
	for (Record *temp in records) {
		if ([[temp name] isEqualToString:name] == true) {
			return temp;
		}
	}

	return nil;
}

- (void) removeRecord:(NSString*)name {
	[records removeObject:[self searchRecord:name]];
}

- (NSString*)description {
	return [NSString stringWithFormat:@"%@", records];
}

@end
