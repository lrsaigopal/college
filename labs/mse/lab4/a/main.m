#import <Foundation/Foundation.h>
#import "record.h"
#import "phonebook.h"

int main ()
{
	//Record* owner = [[Record alloc] initWithName:@"Sathyam M Vellal" initWithEmail:@"sathyam.vellal@gmail.com" initWithPhoneNumber:@"81213436453"];
	//NSLog (@"%@", record);

	Record *record;
	PhoneBook *pb = [[PhoneBook alloc] initWithName:@"My PhoneBook"];
	
	record = [[Record alloc] initWithName:@"Foo Bar" initWithEmail:@"foo@bar.com" initWithPhoneNumber:@"1122334455"];
	[pb addRecord:record];
	record = [[Record alloc] initWithName:@"Fuu Baur" initWithEmail:@"fuu@baur.com" initWithPhoneNumber:@"2233445566"];
	[pb addRecord:record];
	record = [[Record alloc] initWithName:@"Fou Bour" initWithEmail:@"fou@bour.com" initWithPhoneNumber:@"3344556677"];
	[pb addRecord:record];
	
	NSLog (@"%@", pb);
	NSLog (@"%@", [pb searchRecord:@"Fuu Baur"]);
	
	NSLog (@"\n\n");
	record = [[Record alloc] initWithName:@"Fuu Baur" initWithEmail:@"fuu@baur.com" initWithPhoneNumber:@"2233445566"];
	[pb removeRecord:@"Fuu Baur"];
	NSLog (@"%@", pb);
	NSLog (@"%@", [pb searchRecord:@"Fuu Baur"]);

	NSLog (@"\n\n");
	record = [[Record alloc] initWithName:@"Fou Bour" initWithEmail:@"fou@bour.com" initWithPhoneNumber:@"3344556677"];
	NSLog (@"%@", [pb searchRecord:@"Fou Bour"]);

	return 0;
}
