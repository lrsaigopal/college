#import <Foundation/Foundation.h>
#import "item.h"
#import "tax.h"
#import "bill.h"

@implementation FinishedItem
- (id) initWithPricePerUnit:(double)ppu initWithQuantity:(double)amount {
	pricePerUnit = ppu;
	quantity = amount;
	return self;
}

- (double) getPricePerUnit {
	return pricePerUnit;
}

- (double) getQuantity {
	return quantity;
}

- (id) getTaxType {
	return [FinishedTax class];
}

- (id) getBill {
	return [[Bill alloc] initWithItem:self];
}
@end

@implementation GroceryItem
- (id) initWithPricePerUnit:(double)ppu initWithQuantity:(double)amount {
	pricePerUnit = ppu;
	quantity = amount;
	return self;
}

- (double) getPricePerUnit {
	return pricePerUnit;
}

- (double) getQuantity {
	return quantity;
}

- (id) getTaxType {
	return [GroceryTax class];
}

- (id) getBill {
	return [[Bill alloc] initWithItem:self];
}
@end

@implementation Chair : FinishedItem
@end

@implementation Table : FinishedItem
@end

@implementation Rice : GroceryItem
@end

@implementation Wheat : GroceryItem
@end

