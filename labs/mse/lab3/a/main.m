#import <Foundation/Foundation.h>
#import "bill.h"
#import "item.h"

int main ()
{
	NSObject<Item> *i;
	Bill* b;
	i = [[Rice alloc] initWithPricePerUnit:100 initWithQuantity:1];
	b = [i getBill];
	NSLog (@"\nPrice: %lf\nTax: %lf\nTotal: %lf\n", [b getPrice], [b getTax], [b getTotalPrice]);
	
	return 0;
}
