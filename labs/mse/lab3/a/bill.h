#import <Foundation/Foundation.h>
#import "tax.h"
#import "item.h"

@interface Bill : NSObject
{
	@private NSObject<Tax>* taxObject;
	NSObject<Item>* item;
}
- (id) initWithItem:(NSObject<Item>*)i;
- (double) getPrice;
- (double) getTax;
- (double) getTotalPrice;
@end
