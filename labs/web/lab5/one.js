function init() {
    query();
}

function query() {
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            items = xhr.responseText.split(';');
            for (var i = 0; i < 5; ++i) {
                item = document.getElementById('item'+i);
                count = document.getElementById('count'+i);
                info = items[i].split(':');
                item.innerHTML = info[0];
                count.innerHTML = info[1];
            }
        }
    }
    xhr.open("GET", "http://localhost/college/labs/web/lab5/data.txt", true);
    xhr.send();
}

function buy() {
    product = document.getElementById('product');
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            alert(xhr.responseText);
        }
    }
    xhr.open("GET", "http://localhost/college/labs/web/lab5/buy.php?product=" + product.value, true);
//    xhr.send("product=" + product.value);
    xhr.send();
}