function drawFace() {
	canvas = document.getElementById("facecanvas");
	context = canvas.getContext("2d");
	canvas = canvas;
	
	//face
	context.beginPath();
	context.arc(350, 250, 220, 0, Math.PI * 2, false);
	context.fillStyle = "#FFEECC";
	context.fill();
	context.stroke();
	context.closePath();
	
	//left outer eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(340, 200, 25, 0, Math.PI * 2, false);
	context.fillStyle = "#FFFFFF";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//right outer eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(580, 200, 25, 0, Math.PI * 2, false);
	context.fillStyle = "#FFFFFF";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//left inner eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(335, 208, 15, 0, Math.PI * 2, false);
	context.fillStyle = "#000000";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//right inner eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(575, 208, 15, 0, Math.PI * 2, false);
	context.fillStyle = "#000000";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//nose
	context.beginPath();
	context.arc(350, 270, 15, 0, Math.PI * 1.3, false);
	context.strokeStyle = "#000000";
	context.stroke();
	context.closePath();
	
	//mouth
	context.beginPath();
	context.arc(350, 270, 100, Math.PI * 0.2, Math.PI * 0.9, false);
	context.strokeStyle = "#000000";
	context.stroke();
	context.closePath();
	
}

function wink() {
	canvas = document.getElementById("facecanvas");
	context = canvas.getContext("2d");
	canvas = canvas;
	
	//face
	context.beginPath();
	context.arc(350, 250, 220, 0, Math.PI * 2, false);
	context.fillStyle = "#FFEECC";
	context.fill();
	context.stroke();
	context.closePath();
	
	//left outer eye
	context.beginPath();
	context.save();
	context.scale(0.75, 0.7);
	context.arc(340, 290, 25, 0, Math.PI * 2, false);
	context.fillStyle = "#EEDDBB";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//right outer eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(580, 200, 25, 0, Math.PI * 2, false);
	context.fillStyle = "#FFFFFF";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//right inner eye
	context.beginPath();
	context.save();
	context.scale(0.75, 1);
	context.arc(575, 208, 15, 0, Math.PI * 2, false);
	context.fillStyle = "#000000";
	context.fill();
	context.strokeStyle = "#000000";
	context.stroke();
	context.restore();
	context.closePath();
	
	//nose
	context.beginPath();
	context.arc(350, 270, 15, 0, Math.PI * 1.3, false);
	context.strokeStyle = "#000000";
	context.stroke();
	context.closePath();
	
	//mouth
	context.beginPath();
	context.arc(350, 270, 100, Math.PI * 0.2, Math.PI * 0.9, false);
	context.strokeStyle = "#000000";
	context.stroke();
	context.closePath();
	
	setTimeout(drawFace, 250);
}

function init() {
	drawFace();
}