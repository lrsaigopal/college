function init() {
    text = document.getElementById("text");
    size = document.getElementById("fontsize").value;
    color = document.getElementById("fontcolor").value;
    
    text.style.fontSize = window.localStorage.getItem("size");
    text.style.color = window.localStorage.getItem("color");
    window.localStorage.removeItem("size");
    window.localStorage.removeItem("color");
    
    if (typeof("Storage") == undefined) {
        alert("storage not supported");
    }
    window.addEventListener("storage", function() {
        alert("Got storage event");
    }, true);
}

function applyhere() {
    text = document.getElementById("text");
    size = document.getElementById("fontsize").value;
    color = document.getElementById("fontcolor").value;
    
    text.style.fontSize = size+"pt";
    text.style.color = color;
}

function applysave() {
    text = document.getElementById("text");
    size = document.getElementById("fontsize").value;
    color = document.getElementById("fontcolor").value;
    
    window.localStorage["size"] = size+"pt";
    window.localStorage["color"] = color;
}

function applyall() {
    text = document.getElementById("text");
    size = document.getElementById("fontsize").value;
    color = document.getElementById("fontcolor").value;

    window.localStorage.setItem("size", size);
    window.localStorage.setItem("color", color);
    window.localStorage.setItem("applyall", "true");
}