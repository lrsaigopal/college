function init() {
	ballRadius = 20;
	canWidth = 500;
	canHeight = 500;
	ball1Color = "#a52a2a";
	ball2Color = "#2f4f4f";
	dx1 = 2;
	dy1 = 1;
	dx2 = -2;
	dy2 = -1;
	xMin = ballRadius;
	xMax = canWidth - ballRadius;
	yMin = ballRadius;
	yMax = canHeight - ballRadius;
	
	init_canvas();
}

function init_canvas() {
	canvas = document.getElementById("can_ball");
	canvas.width = canvas.width;
	context = canvas.getContext("2d");
	x1 = rand(xMin, xMax);
	x2 = rand(xMin, xMax);
	y1 = rand(yMin, yMax);
	y2 = rand(yMin, yMax);
	
	context.beginPath();
	context.arc(x1, y1, ballRadius, 0, 2 * Math.PI, false);
	context.fillStyle = ball1Color;
	context.fill();
	context.closePath();
	
	context.beginPath();
	context.arc(x2, y2, ballRadius, 0, 2 * Math.PI, false);
	context.fillStyle = ball2Color;
	context.fill();
	context.closePath();
}

function update_canvas() {
	canvas = document.getElementById("can_ball");
	canvas.width = canvas.width;
	context = canvas.getContext("2d");
	
	context.beginPath();
	context.arc(x1, y1, ballRadius, 0, 2 * Math.PI, false);
	context.fillStyle = ball1Color;
	context.fill();
	context.closePath();
	
	context.beginPath();
	context.arc(x2, y2, ballRadius, 0, 2 * Math.PI, false);
	context.fillStyle = ball2Color;
	context.fill();
	context.closePath();
}

function move_ball1() {
	x1 += dx1;
	y1 += dy1;
	
	if (x1 < xMin || x1 > xMax) {
		dx1 *= -1;
	}
	
	if (y1 < yMin || y1 > yMax) {
		dy1 *= -1;
	}
	
	if (Math.abs(x1 - x2) < ballRadius*2 && Math.abs(y1 - y2) < ballRadius) {
		dx1 *= -1;
		dy1 *= -1;
		dx2 *= -1;
		dy2 *= -1;
	}
	
	update_canvas();
	id1 = setTimeout(move_ball1, 5);
}

function move_ball2() {
	x2 += dx2;
	y2 += dy2;
	
	if (x2 < xMin || x2 > xMax) {
		dx2 *= -1;
	}
	
	if (y2 < yMin || y2 > yMax) {
		dy2 *= -1;
	}
	
	if (Math.abs(x1 - x2) < ballRadius && Math.abs(y1 - y2) < ballRadius) {
		dx1 *= -1;
		dy1 *= -1;
		dx2 *= -1;
		dy2 *= -1;
	}
	
	update_canvas();
	id2 = setTimeout(move_ball2, 5);
}

function move() {
	setTimeout(move_ball1, 1);
	setTimeout(move_ball2, 1);
	setTimeout(speed, 10);
}

function stop() {
	clearTimeout(id1);
	clearTimeout(id2);
}

function rand(min, max) {
	return Math.random() * (max - min) + min;
}

function fact_normal() {
	input = document.getElementById("num");
	val = parseInt(input.value);
	
	res = 1;
	while (val > 0) {
		res *= val;
		--val;
	}
	sleep(1);
	
	input.value = res;
}

function fact_worker() {
	input = document.getElementById("num");
	val = parseInt(input.value);
	
	worker = new Worker("calc.js");
	
	worker.onerror = function (event) {
		input.value = "Error...";
	}
	
	worker.onmessage = function (event) {
		input.value = event.data;
	}
	
	worker.postMessage(val);
}

function sleep(delay) {
	var now = new Date();
	var desiredTime = new Date().setSeconds(now.getSeconds() + delay);
	
	while (now < desiredTime) {
		now = new Date(); // update the current time
	}
}